﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManager_taskback.Models
{
    public class CommentsDataModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        
        public UserDataModel User { get; set; }
    }
}
