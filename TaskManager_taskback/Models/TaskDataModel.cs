﻿using System;
using System.Collections.Generic;
using TaskManager_taskback.Utilities;

namespace TaskManager_taskback.Models
{
    public class TaskDataModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public TaskType TaskType { get; set; }
        public TaskProgressStatus TaskStatus { get; set; }
        public ICollection<CommentsDataModel> Comments { get; set; }
    }
}
