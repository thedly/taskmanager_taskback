﻿using System;
using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TaskManager_taskback.Data;
using TaskManager_taskback.Extensions;
using TaskManager_taskback.Services;
using static Microsoft.AspNetCore.Authentication.AzureAdB2CAuthenticationBuilderExtensions;

namespace TaskManager_taskback
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            if (env.IsDevelopment())
                builder.AddUserSecrets<Startup>();

            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(sharedOptions =>
            {
                sharedOptions.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                sharedOptions.DefaultChallengeScheme = OpenIdConnectDefaults.AuthenticationScheme;
            })
            .AddAzureAdB2C(options => {

                Configuration.Bind("Authentication:AzureAdB2C", options);
              
                
                })
            .AddCookie();


            // Add the configuration to DI
            services.AddSingleton(provider => Configuration);
            // Add framework services.
            services.AddOptions();
            // prepare for dependency injection
            services.AddApplicationServices();
            // Adding distributed cache
            services.AddDistributedMemoryCache();
            //In production use Redis for caching
            //services.AddDistributedRedisCache();
            services.AddAuthorization();
            // Adding automapper to map DTO objects to models, viewmodels and vice versa
            services.AddAutoMapper();
            // Add db context 
            services.AddDbContext<TaskMgrDbcontext>(options => options.UseSqlServer(
                Configuration["Azure:Sql:ConnectionString"]
                ));

            //services.AddAuthentication().AddOpenIdConnect(options =>
            //{
            //    options.Authority = Configuration["Authentication:OpenId:Authority"];
            //    options.ClientId = Configuration["Authentication:OpenId:ClientId"];
            //    options.ClientSecret = Configuration["Authentication:OpenId:AppSecret"];
            //});
            services.AddAuthentication().AddFacebook(facebookOptions =>
            {
                facebookOptions.AppId = Configuration["Authentication:Facebook:AppId"];
                facebookOptions.AppSecret = Configuration["Authentication:Facebook:AppSecret"];
            });

            services.AddMvc();
        }

        

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=TaskData}/{action=Index}/{id?}");
            });
        }

        
    }
}
