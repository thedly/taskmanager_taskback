﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager_taskback.Models;
using TaskManager_taskback.Utilities;

namespace TaskManager_taskback.ViewModels
{
    public class TaskDataViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public TaskType TaskType { get; set; }
        public TaskProgressStatus TaskStatus { get; set; }
        public IEnumerable<CommentsDataModel> Comments { get; set; }
    }
}
