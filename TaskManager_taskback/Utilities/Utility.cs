﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManager_taskback.Utilities
{
    public static class Utility
    {

    }

    public enum TaskProgressStatus
    {
        Active,
        InProgress,
        Closed
    }

    public enum TaskType {
        Bug,
        Feature,
        Enhancement
    }

}
