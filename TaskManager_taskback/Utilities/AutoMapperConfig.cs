﻿using AutoMapper;
using TaskManager_taskback.Data.Entities;
using TaskManager_taskback.Models;
using TaskManager_taskback.ViewModels;

namespace TaskManager_taskback.Utilities
{
    // Adapter pattern in place
    public class AutoMapperConfig: Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<TaskDataModel, TaskDataViewModel>();
            CreateMap<TaskDataViewModel, TaskDataModel>();
            CreateMap<TaskDataModel, TaskData>();
            CreateMap<TaskData, TaskDataModel>();


            CreateMap<TaskDataModel, TaskDataViewModel>();
            CreateMap<TaskDataViewModel, TaskDataModel>();
            CreateMap<TaskDataModel, TaskData>();
            CreateMap<TaskData, TaskDataModel>();


            CreateMap<TaskDataModel, TaskDataViewModel>();
            CreateMap<TaskDataViewModel, TaskDataModel>();
            CreateMap<TaskDataModel, TaskData>();
            CreateMap<TaskData, TaskDataModel>();

            CreateMap<UserDataModel, UserData>();
            CreateMap<UserData, UserDataModel>();

        }
    }
}
