﻿using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using TaskManager_taskback.Data.Repositories;
using TaskManager_taskback.Services;
using static Microsoft.AspNetCore.Authentication.AzureAdB2CAuthenticationBuilderExtensions;

namespace TaskManager_taskback.Extensions
{
    public static class DependencyInjectionExtensions
    {
        public static void AddApplicationServices(this IServiceCollection services)
        {
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserRepository, UserRepository>();

            services.AddScoped<ITaskService, TaskService>();
            services.AddScoped<ITaskRepository, TaskRepository>();
        }
    }
}
