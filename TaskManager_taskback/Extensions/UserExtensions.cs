﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace TaskManager_taskback.Extensions
{
    public static class UserExtensions
    {
        public static string GetUserId(this ClaimsPrincipal User)
        {
            return User.Claims.FirstOrDefault(g => g.Type.Equals("http://schemas.microsoft.com/identity/claims/objectidentifier"))?.Value;
        }

        public static string GetUserEmail(this ClaimsPrincipal User)
        {
            return User.Claims.FirstOrDefault(g => g.Type.Equals("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress"))?.Value;
        }
    }
}
