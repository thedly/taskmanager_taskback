﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TaskManager_taskback.Utilities;

namespace TaskManager_taskback.Extensions
{
    public class EnumToListExtension<TEnumType> where TEnumType: struct
    {
        private static readonly Regex _pattern = new Regex(@"
                (?<=[A-Z])(?=[A-Z][a-z]) |
                 (?<=[^A-Z])(?=[A-Z]) |
                 (?<=[A-Za-z])(?=[^A-Za-z])", RegexOptions.IgnorePatternWhitespace);

        public async static Task<IEnumerable<string>> GetListFromEnum()
        {
            return Enum.GetValues(typeof(TEnumType))
            .Cast<TEnumType>()
            .Select(v => SplitValueByCase(v))
            .ToList();
        }

        private static string SplitValueByCase<TEnumType>(TEnumType str) {
            return _pattern.Replace(str.ToString(), " ");
        }
    }
}
