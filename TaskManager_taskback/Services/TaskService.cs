﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager_taskback.Data.Entities;
using TaskManager_taskback.Data.Repositories;
using TaskManager_taskback.Models;
using TaskManager_taskback.Utilities;
using TaskManager_taskback.Extensions;

namespace TaskManager_taskback.Services
{
    public interface ITaskService
    {
        Task<IEnumerable<TaskDataModel>> GetAllTasks();
        Task<int> UpdateTask(TaskDataModel taskDataModel);
        Task<int> DeleteTask(TaskDataModel taskDataModel);
        Task<int> AddNewTask(TaskDataModel taskDataModel);
        Task<int> AddCommentToTask(CommentsDataModel commentsDataModel, int id);
        Task<TaskDataModel> GetTask(int id);
        Task<TaskDataModel> GetTaskWithComments(int id);
        Task<CommentsDataModel> GetCommentById(int taskId, int id);
        Task<int> DeleteCommentFromTask(CommentsDataModel commentsDataModel, int taskId);
        Task<int> UpdateComment(int taskId, CommentsDataModel commentsData);
    } 
    public class TaskService: ITaskService
    {
        private ITaskRepository _taskRepository;
        private IMapper _mapper;
        public TaskService(
            ITaskRepository taskRepository,
            IMapper mapper
            )
        {
            _taskRepository = taskRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<TaskDataModel>> GetAllTasks()
        {
            var tasks = await _taskRepository.GetAll();
            return _mapper.Map<IEnumerable<TaskDataModel>>(tasks);
        }

        public async Task<TaskDataModel> GetTask(int id)
        {
            var task = await _taskRepository.Get(id);
            return _mapper.Map<TaskDataModel>(task);
        }

        public async Task<int> UpdateTask(TaskDataModel taskDataModel)
        {
            var entityToBeEdited = await _taskRepository.Get(taskDataModel.Id);
            taskDataModel.CreatedOn = entityToBeEdited.CreatedOn;
            _mapper.Map(taskDataModel, entityToBeEdited);
            return await _taskRepository.Complete();
        }

        public async Task<int> UpdateComment(int taskId, CommentsDataModel commentsData)
        {
            //var entityToBeEdited = await _taskRepository.GetCommentById(taskId, commentsData.Id);
            var dataEntity = _mapper.Map<CommentsData>(commentsData);
            var task = await _taskRepository.GetTaskWithComments(taskId);
            var entityToBeEdited = task.Comments.FirstOrDefault(g => g.Id == commentsData.Id);
            entityToBeEdited.Description = dataEntity.Description;
            
            return await _taskRepository.Complete();
        }

        public async Task<int> AddNewTask(TaskDataModel taskDataModel)
        {
            var entityToBeEdited = _mapper.Map<TaskData>(taskDataModel);
            entityToBeEdited.CreatedOn = DateTime.UtcNow;
            await _taskRepository.Add(entityToBeEdited);
            return await _taskRepository.Complete();
        }

        public async Task<int> DeleteTask(TaskDataModel taskDataModel)
        {
            var entityToBeEdited = _mapper.Map<TaskData>(taskDataModel);
            _taskRepository.Remove(entityToBeEdited);
            return await _taskRepository.Complete();
        }

        public async Task<TaskDataModel> GetTaskWithComments(int id)
        {
            var taskWithComments = await _taskRepository.GetTaskWithComments(id);
            return _mapper.Map<TaskDataModel>(taskWithComments);
        }
        public async Task<int> AddCommentToTask(CommentsDataModel commentsDataModel, int id) {

            var task = await _taskRepository.GetTaskWithComments(id);
            var commentsEntity = _mapper.Map<CommentsData>(commentsDataModel);
            task.Comments.Add(commentsEntity);
            return await _taskRepository.Complete();
        }
        public async Task<int> DeleteCommentFromTask(CommentsDataModel commentsDataModel, int taskId)
        {
            var task = await _taskRepository.GetTaskWithComments(taskId);
            var commentsEntity = _mapper.Map<CommentsData>(commentsDataModel);
            task.Comments.Remove(commentsEntity);
            return await _taskRepository.Complete();
        }
        public async Task<CommentsDataModel> GetCommentById(int taskId, int id) {
            return await _taskRepository.GetCommentById(taskId, id);
        }
    }
}
