﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager_taskback.Data.Entities;
using TaskManager_taskback.Data.Repositories;
using TaskManager_taskback.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using TaskManager_taskback.Extensions;

namespace TaskManager_taskback.Services
{
    public interface IUserService
    {
        Task<UserDataModel> GetUserData(string id);
        Task<bool> UserExists(string userId);
        Task<int> AddUserData(UserDataModel userDataModel);
        Task<int> RegisterUser(ClaimsPrincipal User);
    }
    public class UserService : IUserService
    {
        
        private IUserRepository _userRepository;
        private IMapper _mapper;
        public UserService(
                IUserRepository userRepository,
                IMapper mapper
            )
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<int> AddUserData(UserDataModel userDataModel)
        {
            var userEntity = _mapper.Map<UserData>(userDataModel);
            await _userRepository.Add(userEntity);
            return await _userRepository.Complete();
        }

        public async Task<UserDataModel> GetUserData(string id)
        {
            var userEntity = await _userRepository.FindOne(g => g.UserId.Equals(id));
            return _mapper.Map<UserDataModel>(userEntity);
        }

        public async Task<int> RegisterUser(ClaimsPrincipal User)
        {

            var userId = User.GetUserId();
            var userExists = await UserExists(userId); ;
            if (!userExists)
            {
                var userModel = new UserData
                {
                    Name = User.Identity.Name,
                    Email = User.GetUserEmail(),
                    UserId = userId
                };
                await _userRepository.Add(userModel);
                return await _userRepository.Complete();
            }
            return 0;
        }

        public async Task<bool> UserExists(string userId)
        {
            var usr = await _userRepository.Find(g => g.UserId.Equals(userId));
            return usr.Count() != 0;
        }
    }
}
