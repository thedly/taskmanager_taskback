﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TaskManager_taskback.Utilities;

namespace TaskManager_taskback.Data.Entities
{
    public class TaskData
    {
        [Key]
        public int Id { get; set; }
        [Required, StringLength(500)]
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public TaskType TaskType { get; set; }
        public TaskProgressStatus TaskStatus { get; set; }
        public ICollection<CommentsData> Comments { get; set; }
    }
}
