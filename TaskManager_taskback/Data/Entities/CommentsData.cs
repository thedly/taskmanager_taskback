﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManager_taskback.Data.Entities
{
    public class CommentsData
    {
        [Key]
        public int Id { get; set; }
        [Required, StringLength(500)]
        public string Description { get; set; }
        [Required]
        public UserData User { get; set; }
    }
}
