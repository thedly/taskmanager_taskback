﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager_taskback.Data.Entities;
using TaskManager_taskback.Models;

namespace TaskManager_taskback.Data.Repositories
{
    public interface IUserRepository : IRepository<UserData> { }

    public class UserRepository: Repository<UserData>, IUserRepository
    {
        public UserRepository(TaskMgrDbcontext Context): base(Context) { }
    }
}
