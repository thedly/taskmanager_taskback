﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TaskManager_taskback.Data.Entities;
using TaskManager_taskback.Models;

namespace TaskManager_taskback.Data.Repositories
{
    public interface ITaskRepository : IRepository<TaskData> {

        Task<TaskData> GetTaskWithComments(int id);
        Task<CommentsDataModel> GetCommentById(int taskId, int id);
    }

    public class TaskRepository : Repository<TaskData>, ITaskRepository
    {
        private IMapper _mapper;
        public TaskRepository(TaskMgrDbcontext context, IMapper mapper) : base(context) { _mapper = mapper; }

        public async Task<TaskData> GetTaskWithComments(int id)
        {
            var task = await (_context as TaskMgrDbcontext).Tasks.Include(g => g.Comments).ThenInclude(g => g.User).FirstOrDefaultAsync(g => g.Id.Equals(id));
            return task;
        }
        public async Task<CommentsDataModel> GetCommentById(int taskId, int id) {
            var task = await (_context as TaskMgrDbcontext).Tasks.Include(g => g.Comments).ThenInclude(g => g.User).FirstOrDefaultAsync(g => g.Id.Equals(taskId));
            var comment = task.Comments.FirstOrDefault(g => g.Id == id);
            return _mapper.Map<CommentsDataModel>(comment);
        }

    }
}
