﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager_taskback.Data.Entities;

namespace TaskManager_taskback.Data
{
    public class TaskMgrDbcontext: DbContext
    {
        public TaskMgrDbcontext(DbContextOptions<TaskMgrDbcontext> options) : base(options) { }

        public DbSet<UserData> Users { get; set; }
        public DbSet<TaskData> Tasks { get; set; }
    }

}
