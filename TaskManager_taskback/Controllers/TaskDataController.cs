﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaskManager_taskback.Extensions;
using TaskManager_taskback.Models;
using TaskManager_taskback.Services;
using TaskManager_taskback.Utilities;
using TaskManager_taskback.ViewModels;

namespace TaskManager_taskback.Controllers
{
    public class TaskDataController : Controller
    {
        private ITaskService _taskService;
        private IMapper _mapper;
        public TaskDataController(
                ITaskService taskService,
                IMapper mapper
            )
        {
            _taskService = taskService;
            _mapper = mapper;
        }
        // GET: TaskData
        public async Task<IActionResult> Index()
        {
            var taskModels = await _taskService.GetAllTasks();

            var taskViewModels = _mapper.Map<IEnumerable<TaskDataViewModel>>(taskModels);
            return View(taskViewModels);
        }

        // GET: TaskData/Details/5
        public async Task<IActionResult> Details(int id)
        {
            var task = await _taskService.GetTaskWithComments(id);
            var taskViewModel = _mapper.Map<TaskDataViewModel>(task);
            return View(taskViewModel);
        }

        // GET: TaskData/Create
        public async Task<IActionResult> Create()
        {
            return View();
        }

        // POST: TaskData/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(TaskDataViewModel model)
        {
            try
            {
                // TODO: Add insert logic here      
                if (ModelState.IsValid)
                {
                    var taskModel = _mapper.Map<TaskDataModel>(model);
                    await _taskService.AddNewTask(taskModel);
                    return RedirectToAction(nameof(Index));
                }
                    
                throw new MissingFieldException();                
            }
            catch
            {
                return View();
            }
        }

        // GET: TaskData/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            var task = await _taskService.GetTask(id);
            var taskViewModel = _mapper.Map<TaskDataViewModel>(task);
            return View(taskViewModel);
        }

        // POST: TaskData/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, TaskDataViewModel model)
        {
            try
            {
                var taskModel = _mapper.Map<TaskDataModel>(model);
                await _taskService.UpdateTask(taskModel);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: TaskData/Delete/5
        public async Task<IActionResult> Delete(int id)
        {

            var task = await _taskService.GetTask(id);
            var taskViewModel = _mapper.Map<TaskDataViewModel>(task);
            return View(taskViewModel);
        }

        // POST: TaskData/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id, TaskDataViewModel collection)
        {
            try
            {
                var taskModel = _mapper.Map<TaskDataModel>(collection);
                await _taskService.DeleteTask(taskModel);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}