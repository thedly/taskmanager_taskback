﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaskManager_taskback.Extensions;
using TaskManager_taskback.Models;
using TaskManager_taskback.Services;

namespace TaskManager_taskback.Controllers
{
    public class CommentsController : Controller
    {
        private IUserService _userService;
        private ITaskService _taskService;
        public CommentsController(
                IUserService userService,
                ITaskService taskService
            )
        {
            _userService = userService;
            _taskService = taskService;
        }
        public ActionResult Index()
        {
            return View();
        }
        // GET: Comments/Details/5
        public async Task<IActionResult> Details(int id)
        {
            return View();
        }

        // GET: Comments/Create
        public async Task<IActionResult> Create()
        {
            return View();
        }

        // POST: Comments/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CommentsDataModel comment, int taskId)
        {
            try
            {
                var userId = User.GetUserId();
                var userObj = await _userService.GetUserData(userId);
                comment.User = userObj;
                await _taskService.AddCommentToTask(comment, taskId);
                return RedirectToAction("Details", "TaskData", new { Id =  taskId});
            }
            catch
            {
                throw;//return RedirectToAction("Details", "TaskData", new { Id = taskId });
            }
        }

        // GET: Comments/Edit/5
        public async Task<IActionResult> Edit(int taskId, int id)
        {
            var comment = await _taskService.GetCommentById(taskId, id);
            return View(comment);
        }

        // POST: Comments/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int taskId, CommentsDataModel model)
        {
            try
            {
                await _taskService.UpdateComment(taskId, model);
                return RedirectToAction("Details", "TaskData", new { Id = taskId });
            }
            catch
            {
                throw;
            }
        }

        // GET: Comments/Delete/5
        
        public async Task<IActionResult> Delete(int taskId, int id)
        {
            var comment = await _taskService.GetCommentById(taskId, id);
            return View(comment);
        }

        // POST: Comments/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id, int taskId, string f = null)
        {
            try
            {
                var comment = await _taskService.GetCommentById(taskId, id);
                await _taskService.DeleteCommentFromTask(comment, taskId);
                return RedirectToAction("Details", "TaskData", new { Id = taskId });
            }
            catch
            {
                return View();
            }
        }
    }
}